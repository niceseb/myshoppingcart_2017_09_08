Scala till checkout
A scala till checkout sample application.

Products

Only two products available for this demo. 1. Apple (0.60p) 2. Orange (0.25p)

Usage

$ sbt

run <product> <product>
Example

> run apple apple orange apple
[ Apple, Apple, Orange, Apple ] => £2.05


Step 2 - Added Offers to products

$ sbt
"Buy one, get one for free on Apples"
> run apple apple => 
[ Apple, Apple ] => £0.60

"3 for the price of 2 on Oranges"
> run orange orange orange
[ Orange, Orange, Orange ] => £0.50



